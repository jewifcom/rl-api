# rl-api
Permite consultar el API de [forecast.io](https://developer.forecast.io/) para obtener el clima y hora de las ciudades: Santiago (CL), Zurich (CH), Auckland (NZ), Sydney (AU), Londres (UK), Georgia (USA) .

## Prerequisitos
* [docker](https://docs.docker.com/install/)
* [docker-compose](https://docs.docker.com/compose/install/)

## Ejecución

```
git clone git@bitbucket.org:jewifcom/rl-api.git
cd rl-api
docker-compose up
```
Posteriormente se debe ejecutar el proyecto [rl-front](https://bitbucket.org/jewifcom/rl-front/src/master/)