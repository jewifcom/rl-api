
export default {
  coordenadas: {
    santiago: {
      latitud: -33.4377968,
      longitud: -70.6504451,
    },
    zurich: {
      latitud: 47.3723941,
      longitud: 8.5423328,
    },
    auckland: {
      latitud: -36.8534665,
      longitud: 174.7655514,
    },
    sydney: {
      latitud: -33.8548157,
      longitud: 151.2164539,
    },
    londres: {
      latitud: 51.5073219,
      longitud: -0.1276474,
    },
    georgia: {
      latitud: 41.6809707,
      longitud: 44.0287382,
    },
  },
  forecast: {
    key: '615a7ab5367855d10da7aecfc961c609',
    endpoint: 'https://api.darksky.net/forecast/',
    useMock: process.env.FORECAST_MOCK === 'true',
  },
  redis: {
    keys: {
      coordinates: 'coordenadas',
      weather: 'forecast',
      error: 'api.errors',
    },
  },
  port: 8090,
};
