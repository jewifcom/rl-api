import pino from 'pino';
import express from 'express';
import http from 'http';
import socketIo from 'socket.io';
import config from './config';
import InitStorage from './store/init-storage';
import { getAvailableCities, putError, getWeather } from './server';

const PORT = process.env.PORT || config.port;

const logger = pino();
const app = express();
const server = http.Server(app);
const io = socketIo(server);

InitStorage.initialize(config.coordenadas);

io.on('connection', (socket) => {
  logger.info(`user connect (${socket.id})`);

  getAvailableCities(socket);
  getWeather(socket);
  putError(socket);
});

server.listen(PORT, () => {
  logger.info(`server listening in http://localhost:${PORT}`);
});
