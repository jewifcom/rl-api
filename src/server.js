import pino from 'pino';
import { availableCities } from './forecast/city';
import onSuccesGetForecast from './forecast/handler';
import { setAsync } from './store/redis-client';
import config from './config';
import getForecast from './forecast/forecast';

const logger = pino();

const getAvailableCities = (socket) => {
  socket.on('getAvailableCities', () => {
    availableCities()
      .then(response => socket.emit('availableCities', response))
      .catch((error) => {
        logger.error(error);
        socket.emit('availableCities', {});
      });
  });
};

const getWeather = (socket) => {
  socket.on('getForecast', (cityName) => {
    logger.info(`request was received to query information about the city: ${cityName}`);
    getForecast(cityName)
      .then(response => onSuccesGetForecast(socket, cityName, response))
      .catch(error => logger.error(error));
  });
};

const putError = (socket) => {
  socket.on('putError', (message) => {
    logger.info(`receiving error: ${message}`);

    const date = new Date();
    setAsync(config.redis.keys.error, date.getTime(), message)
      .catch(error => logger.error(error));
  });
};

export { getAvailableCities, getWeather, putError };
