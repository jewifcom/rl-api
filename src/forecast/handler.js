import pino from 'pino';

const logger = pino();

const onSuccesGetForecast = (socket, cityName, forecastResponse) => {
  const newCityInfo = JSON.stringify({
    city: cityName,
    timezone: forecastResponse.timezone,
    time: forecastResponse.time,
    temperature: forecastResponse.temperature,
  });
  socket.emit('updateCityInfo', newCityInfo);
  logger.info(`emiting new ${cityName} info ${newCityInfo}`);
};

export default onSuccesGetForecast;
