import rp from 'request-promise-native';
import pino from 'pino';
import config from '../config';
import { setAsync } from '../store/redis-client';
import { getGeographicalCoordinates } from './city';

const logger = pino();

const saveForecast = async (cityName, data) => {
  await setAsync(config.redis.keys.weather, cityName, JSON.stringify(data))
    .catch(error => logger.error(error));
};

const getEndpoint = async (cityName) => {
  const coordinates = await getGeographicalCoordinates(cityName);
  logger.info(`geographical coordinates for ${cityName}: ${JSON.stringify(coordinates)}`);
  const exclude = '?exclude=minutely,hourly,daily,alerts,flags&lang=es&units=si';
  const endpoint = `${config.forecast.endpoint}${config.forecast.key}/${coordinates.latitud},${coordinates.longitud}${exclude}`;
  return endpoint;
};

const mockResposeForecast = () => {
  const date = new Date();
  return {
    timezone: 'Asia/Tbilisi',
    currently: { time: date.getTime(), temperature: Math.round(Math.random() * 10) },
  };
};

const getForecast = async (cityName) => {
  const endpoint = await getEndpoint(cityName);
  logger.info(`calling to endpoint ${endpoint}`);

  let forecastResponse;
  if (config.forecast.useMock) {
    forecastResponse = mockResposeForecast();
  } else {
    forecastResponse = JSON.parse(await rp(endpoint));
  }

  saveForecast(cityName, forecastResponse);
  return {
    timezone: forecastResponse.timezone,
    time: forecastResponse.currently.time,
    temperature: forecastResponse.currently.temperature,
  };
};

export default getForecast;
