import { getAsync, keysAsync } from '../store/redis-client';
import config from '../config';

const getGeographicalCoordinates = async (cityName) => {
  const coordinates = await getAsync(config.redis.keys.coordinates, cityName);
  return JSON.parse(coordinates);
};

const availableCities = () => keysAsync(config.redis.keys.coordinates);

export { getGeographicalCoordinates, availableCities };
