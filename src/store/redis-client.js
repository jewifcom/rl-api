import redis from 'redis';
import { promisify } from 'util';

const client = redis.createClient(process.env.REDIS_URL);
const getAsync = promisify(client.hget).bind(client);
const setAsync = promisify(client.hset).bind(client);
const keysAsync = promisify(client.hkeys).bind(client);

export {
  client,
  getAsync,
  setAsync,
  keysAsync,
};
