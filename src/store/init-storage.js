import pino from 'pino';
import { setAsync } from './redis-client';
import config from '../config';

const logger = pino();

export default class InitStorage {
  constructor(storeClient) {
    this.storeClient = storeClient;
  }

  static initialize(coordinateList) {
    Object.keys(coordinateList).forEach((cityName) => {
      if (InitStorage.isCityInCoordinateList(coordinateList, cityName)) {
        InitStorage.setInStore(cityName, coordinateList);
      }
    });
  }

  static isCityInCoordinateList(coordinateList, cityName) {
    return coordinateList && Object.prototype.hasOwnProperty.call(coordinateList, cityName);
  }

  static setInStore(cityName, coordinateList) {
    setAsync(config.redis.keys.coordinates, cityName, JSON.stringify(coordinateList[cityName]))
      .catch(error => logger.error(error));
  }
}
